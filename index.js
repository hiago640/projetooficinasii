const express = require("express");
const session = require("express-session");
const bodyParser = require("body-parser");
const db = require("./src/configs/sequelize");
const auth_session = require("./src/auth/session");
const app = express();
const SECRET = "senha";

app.use(express.static("public"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(
	session({
		secret: SECRET,
		name: "bixcoito",
		cookie: { maxAge: 3600000 },
		resave: false,
		saveUninitialized: false,
	})
);

app.get("/", (req, res) => {
	res.sendFile(__dirname + "/public/html/index.html");
});

app.get("/login", (req, res) => {
	res.sendFile(__dirname + "/public/html/login.html");
});

app.get("/product", (req, res) => {
	res.sendFile(__dirname + "/public/html/products.html");
});

app.get("/cadastro", (req, res) => {
	res.sendFile(__dirname + "/public/html/add_product.html");
});

app.get("/cart", auth_session, (req, res) => {
	res.sendFile(__dirname + "/public/html/carrinho.html");
});

// recriar Banco de Dados caso precise
// db.sequelize.sync({ alter: true }).then(() => {
// 	console.log("Deu certo");
// });

//requerindo as rotas de cada entidade

require("./src/clients/routes")(app);
require("./src/products/routes")(app);
require("./src/carrinho/routes")(app);

app.listen(3000, () => {
	console.log("Servidor aberto");
});

//crud
