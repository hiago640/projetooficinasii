const btnCadastrar = document.querySelector(".btnCadastrar");
const btnEntrar = document.querySelector(".btnEntrar");
const formLogin = document.querySelector(".formLogin");
const formCadastrar = document.querySelector(".formCadastrar");
const btnToast = document.querySelector(".btnToast");

// Para alternar sempre que apertar o botão de cadastrar/entrar
btnCadastrar.addEventListener("click", () => {
	formLogin.style.display = "none";
	formCadastrar.style.display = "flex";
});

btnEntrar.addEventListener("click", () => {
	formCadastrar.style.display = "none";
	formLogin.style.display = "flex";
});

btnToast.addEventListener("click", () => {
	const toast = document.getElementById("toast");
	toast.classList.add("show");
	setTimeout(() => {
		toast.classList.remove("show");
	}, 3000);
});
