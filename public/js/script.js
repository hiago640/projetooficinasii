const h1title = $(".title");
let codUser = window.localStorage;
const adminOptions = document.querySelectorAll(".adminOp");
const userOptions = document.querySelectorAll(".userOp");
const cartOptions = document.querySelectorAll(".cartOp");
const logout = document.querySelector(".logout");
const btnlogin = document.querySelector(".loginbtn");
const subtotal = document.querySelector(".subtotal");

Template = {
	construir: (cargo) => {
		switch (cargo) {
			case "ADMIN": {
				//logout.innerHTML = codUser.getItem("") + " ↴";
				adminOptions.forEach((option) => {
					option.setAttribute("style", "display: inline-block");
				});

				userOptions.forEach((option) => {
					option.setAttribute("style", "display:  inline-block");
				});

				btnlogin.setAttribute("style", "display: none");
				logout.setAttribute("style", "display: inline-block");

				break;
			}
			case "COMUM": {
				//logout.innerHTML = codUser.getItem("nome") + " ↴";
				adminOptions.forEach((option) => {
					option.setAttribute("style", "display: none");
				});

				userOptions.forEach((option) => {
					option.setAttribute("style", "display: inline-block");
				});
				btnlogin.setAttribute("style", "display: none");
				logout.setAttribute("style", "display: inline-block");
				break;
			}
			default: {
				adminOptions.forEach((option) => {
					option.setAttribute("style", "display: none");
				});

				userOptions.forEach((option) => {
					option.setAttribute("style", "display: inline-block");
				});

				cartOptions.forEach((option) => {
					option.setAttribute("style", "display: none");
				});

				btnlogin.setAttribute("style", "display: inline-block");
				if (codUser.getItem("id") == null)
					logout.setAttribute("style", "display: none");
			}
		}
	},
};

Users = {
	add: () => {
		let dados = {};
		dados.name = $("#name").val().toUpperCase();
		dados.email = $("#email").val().toUpperCase();
		dados.birth = $("#birth").val();
		dados.cpf = $("#cpf").val();
		dados.address = $("#address").val().toUpperCase();
		dados.number = $("#number").val();
		dados.password = $("#password").val();
		dados.telephone = $("#telephone").val();

		$.ajax({
			url: "/clients",
			type: "POST",
			data: dados,
			success: (data) => {
				setTimeout(() => {
					window.location = "/login";
				}, 1500);
			},
			dataType: "json",
		});
	},

	logar: () => {
		let dados = {};
		dados.cpf = $("#campoCPF").val();
		dados.password = $("#campoSenha").val();
		$.ajax({
			type: "POST",
			url: "/signIn",
			data: dados,
			success: (data) => {
				setTimeout(() => {
					codUser.clear();
					codUser.setItem("id", data.id);
					codUser.setItem("name", data.name);
					codUser.setItem("role", data.role);
					window.location = "/product";
				}, 300);
			},
			error: () => {
				console.log("error");
			},
			dataType: "json",
		});
	},

	sair: (e) => {
		console.log(e);
		e.preventDefault();

		codUser.clear();
		window.location = "/login";

		$.ajax({
			type: "GET",
			url: "/logout",
			success: () => {
				setTimeout(() => {
					Template.construir("COMUM");
					window.location = "/login";
				}, 500);
			},
		});
	},
};

Products = {
	add: () => {
		let dados = {};
		dados.name = $("#campoNome").val();
		dados.description = $("#campoDescription").val();
		dados.valor = $("#campoValor").val();
		dados.qtd = $("#campoQtd").val();

		$.ajax({
			url: "/product",
			type: "POST",
			data: dados,
			success: (data) => {
				console.log(data);
			},
			dataType: "json",
		});
	},

	findAll: () => {
		$.ajax({
			url: "/products",
			type: "GET",
			data: { nomeVaga: $("#find").val() },
			success: (data) => {
				$("#ProdutosCadastrados").empty();
				for (let vaga of data) {
					Products.template(vaga);
				}
			},
			error: () => {
				console.log("Ocorreu um erro!");
			},
			dataType: "json",
		});
	},

	template: (data) => {
		let card = $("<div></div>")
			.attr("id", "product-" + data.id)
			.attr("class", "card border-dark mb-3");

		let cardHeader = $("<div></div>").attr("class", "card-header");

		let cardbody = $("<div></div>").attr("class", "card-body text-dark");

		let cardTitle = $("<h2></h2>").html(data.name).attr("class", "card-title");

		let cardText = $("<p></p>")
			.html("<i>" + data.description + "</i>")
			.attr("class", "card-text");

		let cardText2 = $("<p></p>")
			.html("R$ " + data.value)
			.attr("class", "card-text");

		const botao = $("<button></button>")
			.attr("class", "botao")
			.html("Adicionar ao carrinho");
		$(botao).on("click", (event) => Carrinho.addProduct(event.target));

		$(card).append(cardHeader);
		$(cardbody).append(cardTitle);
		$(cardbody).append(cardText);
		$(cardbody).append(cardText2);
		$(cardbody).append(botao);
		$(card).append(cardbody);

		$("#ProdutosCadastrados").append(card);
	},
};

Carrinho = {
	findAll: () => {
		$.ajax({
			url: "/pedidos",
			type: "GET",
			data: {
				ClientId: codUser.getItem("id") == null ? -1 : codUser.getItem("id"),
			},
			success: (data) => {
				console.log(data);
				$("#ProdutosCarrinho").empty();
				if (data.length == 0 && codUser.getItem("id") != null) {
					let h1 = $("<h1></h1>").html("Seu carrinho está vazio.");
					$("#ProdutosCarrinho").append(h1);
				}

				for (let product of data) {
					Carrinho.template(product);
				}
			},
			error: () => {
				console.log(codUser.getItem("id")), console.log("Ocorreu um erro!");
			},
			dataType: "json",
		});
	},
	template: (data) => {
		subtotal.innerHTML = "R$ " + data.somatoria;

		let product = $("<div></div>")
			.attr("id", "card-carrinho")
			.attr("class", "card border-success mb-3 border-dark")
			.attr("style", "max-width: 18rem; margin-top: 10px; text-align: center");

		let cardHeader = $("<div></div>")
			.attr("class", "card-header bg-transparent border-success card-title")
			.attr("style", "font-size: 40px;")
			.html(data.name);

		let cardBody = $("<div></div>").attr("class", "card-body text-success");

		let cardText = $("<p></p>")
			.attr("class", "card-text")
			.html("<i>" + data.description + "</i>");

		let cardFooter = $("<div></div>")
			.attr("class", "card-footer bg-transparent border-success")
			.html("R$ " + data.valor);

		$(product).append(cardHeader);
		$(cardBody).append(cardText);
		$(product).append(cardBody);
		$(product).append(cardFooter);

		$("#ProdutosCarrinho").append(product);
	},
	addProduct: (btn) => {
		const product = $(btn).parent().parent();
		const id = $(product).attr("id").replace("product-", "");

		if (!codUser.getItem("id")) {
			setTimeout(() => {
				window.location = "/login";
			}, 500);
		}

		$.ajax({
			url: "/cart",
			type: "POST",
			data: {
				ClientId: codUser.getItem("id"),
				ProductId: id,
				total: 10,
			},
			success: (data) => {
				alert("Produto adicionado ao carrinho");
			},
			error: (e) => {
				//alert(e.responseText);
			},
			dataType: "json",
		});
	},
};

$(".logout").click((event) => {
	console.log("dentro do logout");
	Users.sair(event);
	event.preventDefault();
});

$(document).ready(() => {
	Products.findAll();
	if (!codUser.getItem("id")) return Template.construir("");

	Template.construir(codUser.getItem("role"));
});
