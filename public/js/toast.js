
const btnToast = document.querySelector(".btnToast");

btnToast.addEventListener("click", () => {
	const toast = document.getElementById("toast");
	toast.classList.add("show");
	setTimeout(() => {
		toast.classList.remove("show");
	}, 3000);
});
