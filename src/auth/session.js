module.exports = (req, res, next)=>{
    if(!req.session.user){
        console.log("Sessão inexistente ou finalizada");
        return res.redirect('/login');
    }
    return next();
}