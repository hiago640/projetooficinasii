const db = require("../configs/sequelize");
const Cart = require("./model");
const { Op } = db.Sequelize;

exports.create = (req, res) => {
	Cart.create({
		ProductId: req.body.ProductId,
		ClientId: req.body.ClientId,
		total: req.body.total,
	})
		.then((cart) => res.send(cart))
		.catch((err) => err);
};

exports.findAll = (req, res) => {
	if (req.query.ClientId != null) {
		Cart.sequelize
			.query(
				`SELECT P.name, P.description, P.value as valor, C.total, 
                (SELECT SUM(P.VALUE) FROM CARRINHOS T
                  JOIN products P ON P.ID = T.PRODUCTID
                  WHERE T.ClientId = C.ClientId ) somatoria
               FROM CARRINHOS C JOIN PRODUCTS P ON C.PRODUCTID = P.ID  WHERE C.ClientId = ${req.query.ClientId}`,
				{
					model: Cart,
				}
			)
			.then(function (results, metadata) {
				// console.log(results);
				res.send(results);
			});
	} else {
		Cart.sequelize
			.query(
				`SELECT P.name, P.description, P.value, C.total FROM CARRINHOS C JOIN PRODUCTS P ON C.PRODUCTID = P.ID WHERE 1 = 0`,
				{
					model: Cart,
				}
			)
			.then(function (results, metadata) {
				// console.log(results);
				res.send(results);
			});
	}
};

// exports.findAll = (req, res) => {
// 	Cart.findAll().then((user) => res.send(user));
// };
