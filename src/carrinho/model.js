const db = require("../configs/sequelize");
const { Sequelize, Model, DataTypes } = db.Sequelize;
const Client = require("../clients/model");
const Product = require("../products/model");
const sequelize = db.sequelize;

class Carrinho extends Model {}

//declarando os campos da tabela Carrinho
Carrinho.init(
	{
		total: {
			type: DataTypes.DECIMAL(10, 2),
		},

	},
	{ sequelize }
);

Product.belongsToMany(Client, {
	through: Carrinho,
	onDelete: "cascade",
	onUpdate: "cascade",
});

Client.belongsToMany(Product, {
	through: Carrinho,
	onDelete: "cascade",
	onUpdate: "cascade",
});

module.exports = Carrinho;
