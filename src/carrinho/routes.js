const session = require("../auth/session");
module.exports = (app) => {
	const controller = require("./controller");

	//rota post para criar novos usuários
	app.post("/cart", session, controller.create);
	// /*
	// 	//rota get para retornar todos os usuários
	app.get("/pedidos", session, controller.findAll);

};
