const db = require("../configs/sequelize");
const Client = require("./model");
const bcrypt = require("bcrypt");
const { Op } = db.Sequelize;

exports.create = async (req, res) => {
	let pass = await bcrypt.hash(req.body.password, 10);

	try {
		let client = await Client.create({
			name: req.body.name,
			email: req.body.email,
			birth: req.body.birth,
			cpf: req.body.cpf,
			address: req.body.address,
			number: req.body.number,
			password: pass,
			telephone: req.body.telephone,
			role: "COMUM",
		});
		res.send(client);
	} catch (err) {
		console.log(err);
	}
};

exports.findAllAux = (req, res) => {
	Client.findAll().then((Clients) => res.send(Clients));
};

exports.findAll = async (req, res) => {
	try {
		let client = await Client.findAll();
		res.send(client);
	} catch (err) {
		console.log(err);
	}
};
exports.login = async (req, res) => {
	try {
		let user = await Client.findOne({ where: { cpf: req.body.cpf } });
		if (user) {
			bcrypt.compare(req.body.password, user.password, function (err, result) {
				if (result) {
					req.session.user = user.id;
					res.json(user);
				} else {
					res.send("Senha inválida");
				}
			});
		} else {
			res.send("Usuário inválido");
		}
	} catch (err) {}
};

exports.findUser = async (login) => {
	try {
		let user = await Client.findOne({
			where: { cpf: login },
		});
		return user;
	} catch (err) {
		console.log(err.message);
	}
	return null;
};

exports.validPassword = (password, client) => {
	return bcrypt.compareSync(password, client.password);
};

exports.findById = async (id) => {
	return Client.findByPk(id);
};

exports.logout = async (req, res) => {
	console.log("deslogado");
	//req.session.destroy((err) => res.redirect("/"));
};
