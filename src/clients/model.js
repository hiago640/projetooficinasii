const db = require("../configs/sequelize");
const { Model, DataTypes } = db.Sequelize;
const sequelize = db.sequelize;

class Client extends Model {}

//declarando os campos da tabela Clients
Client.init(
	{
		name: {
			type: DataTypes.STRING,
		},
		email: {
			type: DataTypes.STRING,
		},
		birth: {
            type: DataTypes.STRING,
			//type: DataTypes.DATE,
		},
		cpf: {
			type: DataTypes.STRING,
			unique: true,
		},
        address: {
			type: DataTypes.STRING,
		},
        number: {
			type: DataTypes.INTEGER,
		},
		password: {
			type: DataTypes.STRING,
		},
		telephone: {
			type: DataTypes.STRING,
		},
		role: {
			type: DataTypes.STRING,
		},
	},
	{ sequelize }
);

//exporta o modelo da tabela pra ser usado no controller
module.exports = Client;
