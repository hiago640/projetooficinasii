module.exports = (app) => {
	const controller = require("./controller");

	app.post("/clients", controller.create);
	app.post("/signIn", controller.login);
	app.get("/logout", controller.logout);
};
