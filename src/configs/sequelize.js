//aqui fica a parte responsável pela instancia do Sequelize

const dbConfig = require("./database");
const Sequelize = require("Sequelize");
const sequelize = new Sequelize(dbConfig);

//cria a variável para exportar

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

module.exports = db; //exporta a variável criada.
