const db = require("../configs/sequelize");
const Product = require("./model");
const { Op } = db.Sequelize;

exports.create = async (req, res) => {
	try {
		let product = await Product.create({
			name: req.body.name,
			qtd: req.body.qtd,
			description: req.body.description,
			value: req.body.valor,
		});
		res.send(product);
	} catch (err) {
		console.log(err);
	}
};

exports.findAllAux = (req, res) => {
	Product.findAll().then((Product) => res.send(Product));
};


exports.findAll = (req, res) => {
	console.log("export findall");
	Product.findAll({
		where: { name: { [Op.like]: "%" + req.query.nomeVaga + "%" } },
		order: ["createdAt"],
	}).then((products) => res.send(products));
};
