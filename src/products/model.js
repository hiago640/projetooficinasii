const db = require("../configs/sequelize");
const { Model, DataTypes } = db.Sequelize;
const sequelize = db.sequelize;

class Product extends Model {}

Product.init(
	{
		name: {
			type: DataTypes.STRING,
		},
	    qtd: {
			type: DataTypes.INTEGER,
		},
		description: {
			type: DataTypes.STRING,
		},
        value: {
            type: DataTypes.DECIMAL(10, 2)
        }
	},
	{
		sequelize,
	}
);

module.exports = Product;
