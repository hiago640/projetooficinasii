const session = require('../auth/session')
module.exports = (app) => {
	const controller = require("./controller");

	app.post("/product", session, controller.create);
	app.get("/products", controller.findAll);
	
};
